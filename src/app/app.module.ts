import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormTestComponent } from './form-test/form-test.component';
import { InputFormComponent } from './input-form/input-form.component';
import { SubmitButtonComponent } from './submit-button/submit-button.component';
import { TextareaFormComponent } from './textarea-form/textarea-form.component';
import { ColorFormButtonComponent } from './color-form-button/color-form-button.component';
import { ColorFormButtonContainerComponent } from './color-form-button-container/color-form-button-container.component';
import { InputFileFormComponent } from './input-file-form/input-file-form.component';
import { DatePickerFormComponent } from './date-picker-form/date-picker-form.component';
import { MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



@NgModule({
  declarations: [
    AppComponent,
    FormTestComponent,
    InputFormComponent, 
    SubmitButtonComponent, 
    TextareaFormComponent,
    ColorFormButtonComponent,
    ColorFormButtonContainerComponent,
    DatePickerFormComponent,
    InputFileFormComponent, 
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatDatepickerModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatNativeDateModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
