import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-color-form-button',
  templateUrl: './color-form-button.component.html',
  styleUrls: ['./color-form-button.component.css']
})
export class ColorFormButtonComponent implements OnInit {

  @Input() color: any =''
  constructor() { }

  ngOnInit(): void {
  }

}
