import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ColorFormButtonComponent } from './color-form-button.component';

describe('ColorFormButtonComponent', () => {
  let component: ColorFormButtonComponent;
  let fixture: ComponentFixture<ColorFormButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ColorFormButtonComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ColorFormButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
