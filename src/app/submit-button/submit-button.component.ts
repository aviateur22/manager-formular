import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-submit-button',
  templateUrl: './submit-button.component.html',
  styleUrls: ['./submit-button.component.css']
})
export class SubmitButtonComponent implements OnInit {
  // text du button de soumission
  @Input() submitButtonText: string = '';

  constructor() { }

  ngOnInit(): void {
  }

}
