import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-color-form-button-container',
  templateUrl: './color-form-button-container.component.html',
  styleUrls: ['./color-form-button-container.component.css']
})
export class ColorFormButtonContainerComponent implements OnInit {

  constructor() { }
  // tableau de couleur disponible
  colors: Array<any> = [ 
    {color: "red", class: "button--red"},
    {color: "blue", class: "button--blue"},
    {color: "orange", class: "button--orange"},
    {color: "pink", class: "button--pink"},
    {color: "lightPink", class: "button--light-pink"},
    {color: "lightBlue", class: "button--light-blue"},
    {color: "lightOrange", class: "button--light-orange"},
  ];
  ngOnInit(): void {
  }

}
