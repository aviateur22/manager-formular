import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ColorFormButtonContainerComponent } from './color-form-button-container.component';

describe('ColorFormButtonContainerComponent', () => {
  let component: ColorFormButtonContainerComponent;
  let fixture: ComponentFixture<ColorFormButtonContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ColorFormButtonContainerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ColorFormButtonContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
