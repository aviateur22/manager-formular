import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-textarea-form',
  templateUrl: './textarea-form.component.html',
  styleUrls: ['./textarea-form.component.css']
})
export class TextareaFormComponent implements OnInit {

  // label du textarea
  @Input() inputLabelText:string = '';

  //Placeholder textarea
  @Input() textareaPlaceholderText: string ='';

  constructor() { }

  ngOnInit(): void {
  }

}
