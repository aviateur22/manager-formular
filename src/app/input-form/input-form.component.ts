import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-input-form',
  templateUrl: './input-form.component.html',
  styleUrls: ['./input-form.component.css']
})
export class InputFormComponent implements OnInit {
  // label de l'input
  @Input() inputLabelText:string = '';

  //PlaceHolder de l'Input
  @Input() inputPlaceholderText: string = ''

  constructor() { }

  ngOnInit(): void {
  }

}
